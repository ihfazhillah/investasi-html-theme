module.exports = {
  important: true,
  theme: {
    fontFamily: {
      display: ["Exo", "sans-serif"],
      body: ["Noto Sans", "sans-serif"]
    },
    container: {
      center: true
    },
    extend: {
      colors:{
        primary: "#c5262b",
        accent: "#0fc5ff",
        "primary-light": "#f6e0e1ff"
      }
    }
  },
  variants: {},
  plugins: []
}
